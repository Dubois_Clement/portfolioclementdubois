import java.sql.SQLOutput;
import java.util.*;
import java.lang.*;
import static java.lang.Integer.parseInt;
import java.io.IOException; // MODIFICI
import java.io.BufferedReader; // MODIFICI
import java.io.FileReader; // MODIFICI


public class SudokuBase {

    //.........................................................................
    // Fonctions utiles
    //.........................................................................


    /** pré-requis : min <= max
     *  résultat :   un entier saisi compris entre min et max, avec re-saisie éventuelle jusqu'à ce qu'il le soit
     */
    public static int saisirEntierMinMax(int min, int max){
        int n = min; // initialisation bidon
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("Entrez une valeur comprise entre " + min + " et " + max);
            n = parseInt(scanner.next());

        } while (n < min || n > max);

        return n;


    }  // fin saisirEntierMinMax
    //.........................................................................


    /** pré-requis : aucun
     *  résultat : une copie de mat
     *
     */
    public static void copieMatrice(int [][] mat1, int[][] mat2){
        for(int i=0; i<mat1.length; i++){
            for(int j=0; j<mat1[0].length; j++){
                mat2[i][j]=mat1[i][j];
            }
        }

        //________________________________________________________

    }  // fin copieMatrice

    //.........................................................................


    /** pré-requis :  n >= 0
     *  résultat : un tableau de booléens représentant le sous-ensemble de l'ensemble des entiers
     *             de 1 à n égal à lui-même
     */
    public static boolean[] ensPlein(int n){
        boolean[] T=new boolean[n+1];  // initialisation d'un tableau de booléen
        for(int i=1; i<=n; i++){
            T[i]=true;               // met tous les éléments du tableau à vrai afin de simuler un tableau plein
        }
        return T;

        //_____________________________________*/

    }  // fin ensPlein

    //.........................................................................


    /** pré-requis : 1 <= val < ens.length
     *  action :     supprime la valeur val de l'ensemble représenté par ens, s'il y est
     *  résultat :   vrai ssi val était dans cet ensemble
     */
    public static boolean supprime(boolean[] ens, int val){

        if (ens[val] == false) { // si la valeur d'index val dans ens[] est faux
            return false;
        }
        else {
            ens[val] = false;
            return true;
        }

    }  // fin supprime

    //.........................................................................


    /** pré-requis : l'ensemble représenté par ens n'est pas vide
     *  résultat :   un élément de cet ensemble
     */
    public static int uneValeur(boolean[] ens){
        //_____________________________________________
        for (int i = 1; i < ens.length; i++) {
            if (ens[i] == true) {
                return i; // retourne le premier élément true de l'ensemble
            }
        }
        return 0; // retourne 0 si aucun élément de l'ensemble n'est true

    }  // fin uneValeur

    //.........................................................................

    /**

     1 2 3 4 5 6 7 8 9
     --------------------
     1 |6 2 9|7 8 1|3 4 5|
     2 |4 7 3|9 6 5|8 1 2|
     3 |8 1 5|2 4 3|6 9 7|
     -------------------
     4 |9 5 8|3 1 2|4 7 6|
     5 |7 3 2|4 5 6|1 8 9|
     6 |1 6 4|8 7 9|2 5 3|
     -------------------
     7 |3 8 1|5 2 7|9 6 4|
     8 |5 9 6|1 3 4|7 2 8|
     9 |2 4 7|6 9 8|5 3 1|
     -------------------


     1 2 3 4 5 6 7 8 9
     -------------------
     1 |6 0 0|0 0 1|0 4 0|
     2 |0 0 0|9 6 5|0 1 2|
     3 |8 1 0|0 4 0|0 0 0|
     -------------------
     4 |0 5 0|3 0 2|0 7 0|
     5 |7 0 0|0 0 0|1 8 9|
     6||0 0 0|0 7 0|0 0 3|
     -------------------
     7 |3 0 0|0 2 0|9 0 4|
     8 |0 9 0|0 0 0|7 2 0|
     9 |2 4 0|6 9 0|0 0 0|
     -------------------


     * pré-requis : 0<=k<=3 et g est une grille k^2xk^2 dont les valeurs sont comprises
     *              entre 0 et k^2 et qui est partitionnée en k sous-carrés kxk
     * action : affiche la  grille g avec ses sous-carrés et avec les numéros des lignes
     *          et des colonnes de 1 à k^2.
     * Par exemple, pour k = 3, on obtient le dessin d'une grille de Sudoku
     *
     */

    public static void afficheGrille(int k,int[][] g){
        String affichage = "";

        for (int i = 1; i <= k*k; i++) {
            if (i != k*k) {
                affichage = affichage + i + " "; // affiche les chiffres au début
            }
            else {
                affichage = affichage + i; // affiche le dernier chiffre des chiffres au début
            }
        }

        String tirets = "-";

        for (int i = 0; i < k*k; i++) {
            tirets = tirets + "--"; // affiche les chiffres au début
        }

        affichage = affichage + "\n" + tirets;

        //-------------------------------------------------------------------------------------
        // Vrai début du programme

        for (int i = 0; i < k * k; i++) {

            affichage = affichage + "\n" + (i+1) + " |";

            for (int j = 0; j < k * k; j++) {
                if ((j+1) % k == 0) {
                    affichage = affichage + g[i][j] + "|";
                } else {
                    affichage = affichage + g[i][j] + " ";
                }
            }
            if ((i+1) % k == 0) {
                affichage = affichage + "\n" + tirets;
            }
        }
        System.out.println(affichage);

    }


    // fin afficheGrille
    //.........................................................................

    /** pré-requis : k > 0, 0 <= i< k^2 et 0 <= j < k^2
     *  résultat : (i,j) étant les coordonnées d'une case d'une grille k^2xk^2 partitionnée
     *             en k sous-carrés kxk, retourne les coordonnées de la case du haut à gauche
     *             du sous-carré de la grille contenant cette case.
     *  Par exemple, si k=3, i=5 et j=7, la fonction retourne (3,6).
     */
    public static int[] debCarre(int k,int i,int j) { //code à revoir

        // je crée un tableau où chaque élément à la coordonnée de la valeur en haut à gauche de son carré

        int[][][] tabIndices = new int[k*k][k*k][2];
/**
 {     1  2  3  4  5  6  7  8  9
 1 {6, 2, 9, 7, 8, 1, 3, 4, 5},
 2 {4, 7, 3, 9, 6, 5, 8, 1, 2},
 3 {8, 1, 5, 2, 4, 3, 6, 9, 7},
 4 {9, 5, 8, 3, 1, 2, 4, 7, 6},
 5 {7, 3, 2, 4, 5, 6, 1, 8, 9},
 6 {1, 6, 4, 8, 7, 9, 2, 5, 3},
 7 {3, 8, 1, 5, 2, 7, 9, 6, 4},
 8 {5, 9, 6, 1, 3, 4, 7, 2, 8},
 9 {2, 4, 7, 6, 9, 8, 5, 3, 1}
 };
 */
        boolean execute = true;
        int x = 0;
        int y = 0;
        int l = 0;
        int tabI = 0;
        int tabJ = 0;

        while (l < k) { // sur l'axe y de k en k, jusqu'a k (VERIFIER QUE LA VALEUR DE L CHANGE)

            for (int n = 0; n < k; n++) { // sur l'axe y de 1 en 1 jusqu'a k

                while (tabJ < (k*k)) { // sur l'axe x de 1 en 1 jusqu'a k
                    tabIndices[tabI][tabJ][0] = y;
                    tabIndices[tabI][tabJ][1] = x;

                    if ((tabJ+1) % k == 0) { // de 3 en 3 sur l'axe x
                        x = x + 3; // je passe au tableau suivant en restant sur la même ligne
                    }
                    tabJ++; // je passe à la colonne suivante
                }

                tabI++; // je passe à la ligne suivante
                tabJ = 0; // je met tab J à 0 puisque c'est une nouvelle ligne
                x = 0; // pareil, nouvelle ligne donc les colonnes se mettent à 0

            }

            y = y + 3;
            l++;

        }

        return tabIndices[i][j];

    }  // fin debCarre


    //.........................................................................

    // Initialisation
    //.........................................................................


    /** MODIFICI
     *  pré-requis : gComplete est une matrice 9X9
     *  action   :   remplit gComplete pour que la grille de Sudoku correspondante soit complète
     *  stratégie :  les valeurs sont données directement dans le code et on peut utiliser copieMatrice pour mettre à jour gComplete
     */
    public static void initGrilleComplete(int [][] gComplete){
        //_________________________________________________
        int[][] mat = {
                {6, 2, 9, 7, 8, 1, 3, 4, 5},
                {4, 7, 3, 9, 6, 5, 8, 1, 2},
                {8, 1, 5, 2, 4, 3, 6, 9, 7},
                {9, 5, 8, 3, 1, 2, 4, 7, 6},
                {7, 3, 2, 4, 5, 6, 1, 8, 9},
                {1, 6, 4, 8, 7, 9, 2, 5, 3},
                {3, 8, 1, 5, 2, 7, 9, 6, 4},
                {5, 9, 6, 1, 3, 4, 7, 2, 8},
                {2, 4, 7, 6, 9, 8, 5, 3, 1}
        };

        copieMatrice(mat, gComplete);

    } // fin initGrilleComplete

    /**
     * Donnée : un chemin vers un fichier texte de 9 lignes d'entiers séparés par un espace
     * Résultat : retourne une matrice d'entiers 9x9 remplie à partir du fichier ou null en cas d'erreur
     */
    public static int[][] lireGrilleDeSudoku(String fichier) {
        int[][] grille = new int[9][9];

        try (BufferedReader lecteur = new BufferedReader(new FileReader(fichier))) {
            for (int i=0;i<9;i++){
                String ligne=lecteur.readLine();
                String[] valeurs = ligne.split("\\s+");
                for (int j = 0; j < 9 ; j++) grille[i][j] = Integer.parseInt(valeurs[j]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return grille;
    }

    //.........................................................................

    /** MODIFICI
     *  pré-requis : gSecret est une grille de Sudoku complète de mêmes dimensions que gIncomplete et 0 <= nbTrous <= 81
     *  action :     modifie gIncomplete pour qu'elle corresponde à une version incomplète de la grille de Sudoku gSecret (gIncomplete peut être complétée en gSecret),
     *               avec nbTrous trous à des positions aléatoires
     */
    public static void initGrilleIncomplete(int nbTrous, int [][] gSecret, int[][] gIncomplete){
        copieMatrice(gSecret, gIncomplete); //copie de gSecret dans gIncomplete
        while (nbTrous > 0) { // Faire effacer des valeurs de la grille de sudoku tant que le nombre de trous n'est pas atteint
            Random rand = new Random();  //Définit une valeur aléatoire pour positionner les trous
            int i = rand.nextInt(9);
            int j = rand.nextInt(9);

            if (gIncomplete[i][j]!=0){ //st si les adresse du tableau n'ont pas été utilisé deux fois (si est utilisé 2 fois il y a déjà un 0)
                gIncomplete[i][j]=0;
                nbTrous--;
            }
            // Diminuer le nombre de trous de 1
        }
    }// Attention : il ne faut pas qu'il y est deux fois le même nombre aléatoire de i et j car le trou y sera déjà présent


    //.........................................................................

        /** MODIFICI
         *  pré-requis : 0 <= nbTrous <= 81 ; g est une grille 9x9 (vide a priori)
         *  action :   remplit g avec des valeurs saisies au clavier comprises entre 0 et 9
         *               avec exactement nbTrous valeurs nulles
         *               et avec re-saisie jusqu'à ce que ces conditions soient vérifiées.
         *               On suppose dans la version de base que la grille saisie est bien une grille de Sudoku incomplète.
         *  stratégie : utilise la fonction saisirEntierMinMax
         */
        public static int [][] saisirGrilleIncomplete(int nbTrous , int[][]g) {
            while (nbTrous != 0) {
                for (int i = 0; i < g.length; i++) {        //parcours la grille
                    for (int j = 0; j < g.length; j++) {
                        g[i][j] = saisirEntierMinMax(0, 9);  // permet de saisir les nombres dans la grille
                        System.out.println("Il vous reste " + nbTrous + " trou(s) à mettre."); //message à l'utilisateur sur le nombre de trous restant.
                        if (g[i][j] == 0) {
                            nbTrous--; //Diminuer le nombre de trous de 1
                        }
                    }
                }
            }
            return g;
    }  // fin saisirGrilleIncomplete

    //Explication de la fonction contrairement à la fonction initGrilleIncomplete c'est l'utilisateur qui décide où sont les trous

    //.........................................................................



    /** pré-requis : gOrdi est une grille de Sudoku incomplète,
     *               valPossibles est une matrice 9x9 de tableaux de 10 booléens
     *               et nbValPoss est une matrice 9x9 d'entiers
     *  action : met dans valPossibles l'ensemble des entiers de 1 à 9 pour chaque trou de gOrdi
     *           et leur nombre dans nbValPoss
     */
    public static void initPleines(int [][] gOrdi, boolean[][][] valPossibles, int [][] nbValPoss){
        //________________________________________________________________________________________________
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) { // petite boucle de base, on connait

                for (int k = 0; k < 10; k++) { // pour toutes les possibilités de cette case

                    valPossibles[i][j][k] = true; // on met que toutes les valeurs sont possibles
                }

                nbValPoss[i][j] = 9;
            }
        }

    }  // fin initPleines

    //.........................................................................


    /** pré-requis : gOrdi est une grille de Sudoku incomplète,
     *               0<=i<9, 0<=j<9,g[i][j]>0,
     *               valPossibles est une matrice 9x9 de tableaux de 10 booléens
     *               et nbValPoss est une matrice 9x9 d'entiers
     *  action : supprime dans les matrices valPossibles et nbValPoss la valeur gOrdi[i][j] pour chaque case de la ligne,
     *           de la colonne et du carré contenant la case (i,j) correspondant à un trou de gOrdi.
     */
    public static void suppValPoss(int [][] gOrdi, int i, int j, boolean[][][] valPossibles, int [][]nbValPoss){

        // ici on fait pour la ligne

        for (int k = 0; k < gOrdi.length; k++) { // pour tous les éléments de la ligne i

            if (supprime(valPossibles[k][j], gOrdi[i][j])) { // si la valeur de la case de coordonnée i, k est une valeur possible, j'enlève la valeur de la case de coordonnée i, k de valPossibles
                nbValPoss[k][j]--; // j'enlève un aux valeurs possibles
            }
        }

        // ici on fait pour la colonne

        for (int k = 0; k < gOrdi.length; k++) { // pour tous les éléments de la colonne j

            if (supprime(valPossibles[i][k], gOrdi[i][j])) { // si la valeur de la case de coordonnée i, k est une valeur possible, j'enlève la valeur de la case de coordonnée i, k de valPossibles
                nbValPoss[i][k]--; // j'enlève un aux valeurs possibles
            }
        }
        // ici on fait le carré de 3 * 3

        int[] coords = debCarre(3, i, j); // renvoie la coordonnée de la case en haut à gauche du carré de i, j

        for (int k = coords[0]; k < (coords[0]+3); k++) { // pour chaque ligne de notre carré

            for (int l = coords[1]; l < (coords[1]+3); l++) { // pour chaque colonne de chaque ligne du coup

                if (supprime(valPossibles[k][l], gOrdi[i][j])) { // si la valeur de la case de coordonnée i, k est une valeur possible, j'enlève la valeur de la case de coordonnée i, k de valPossibles
                    nbValPoss[k][l]--; // j'enlève un aux valeurs possibles
                }
            }

        }
        System.out.println("supValPoss check\n");
    }  // fin suppValPoss


    //.........................................................................

    /** pré-requis : gOrdi est une grille de Sudoju incomplète,
     *               valPossibles est une matrice 9x9 de tableaux de 10 booléens
     *               et nbValPoss est une matrice 9x9 d'entiers
     * action :      met dans valPossibles l'ensemble des valeurs possibles de chaque trou de gOrdi
     *               et leur nombre dans nbValPoss
     */
    public static void initPossibles(int [][] gOrdi, boolean[][][] valPossibles, int [][]nbValPoss) {
        // c'est pas de la triche si je me copie moi même AHAHAHAHAHAH
        // ici on fait pour la ligne

        initPleines(gOrdi, valPossibles, nbValPoss);

        for (int i = 0; i < 9; i++) { // pour tous les éléments de la ligne i
            for (int j = 0; j < 9; j++) { // pour tous les éléments de la colonne j

                    // ici on fait pour la ligne

                    for (int k = 0; k < 9; k++) { // pour tous les éléments de la ligne i

                        if (gOrdi[k][j] != 0 && valPossibles[i][j][gOrdi[i][k]]) { // si k, i n'est pas un trou et que la valeur n'est pas déjà true
                            valPossibles[i][j][gOrdi[k][j]] = false; // je l'enlève de mes valeurs possibles pour cette case
                            nbValPoss[i][j]--;
                        }
                    }

                    // ici on fait pour la colonne

                    for (int k = 0; k < 9; k++) { // pour tous les éléments de la colonne j

                        if (gOrdi[i][k] != 0 && valPossibles[i][j][gOrdi[i][k]]) { // si i, k n'est pas un trou et que la valeur n'est pas déjà true
                            valPossibles[i][j][gOrdi[i][k]] = false; // je l'enlève de mes valeurs possibles pour cette case
                            nbValPoss[i][j]--;
                        }
                    }

                    // ici on fait le carré de 3 * 3

                    int[] coords = debCarre(3, i, j); // renvoie la coordonnée de la case en haut à gauche du carré de i, j

                    for (int k = coords[0]; k < (coords[0]+3); k++) { // pour chaque ligne de notre carré

                        for (int l = coords[1]; l < (coords[1]+3); l++) { // pour chaque colonne de chaque ligne du coup

                            if (gOrdi[k][l] != 0 && valPossibles[i][j][gOrdi[k][l]]) { // si k, l n'est pas un trou et que la valeur n'est pas déjà true
                                valPossibles[i][j][gOrdi[k][l]] = false; // je l'enlève de mes valeurs possibles pour cette case
                                nbValPoss[i][j]--;
                            }
                        }

                    }
            }
        }

        System.out.println("initpossible chck");


    }  // fin initPossibles

    //.........................................................................


    /** pré-requis : gSecret, gHumain et gOrdi sont des grilles 9x9
     *  action :     demande au joueur humain de saisir le nombre nbTrous compris entre 0 et 81,
     *               met dans gSecret une grille de Sudoku complète,
     *               met dans gHumain une grille de Sudoku incomplète, pouvant être complétée en gSecret
     *               et ayant exactement nbTrous trous de positions aléatoires,
     *               met dans gOrdi une grille de Sudoku incomplète saisie par le joueur humain
     *               ayant  nbTrous trous,
     *               met dans valPossibles l'ensemble des valeurs possibles de chaque trou de gOrdi
     *               et leur nombre dans nbValPoss.
     * retour : la valeur de nbTrous
     */
    public static int initPartie(int [][] gSecret, int [][] gHumain, int [][] gOrdi,boolean[][][] valPossibles, int [][]nbValPoss){
        int nbTrous = saisirEntierMinMax(0, 81);
        initGrilleComplete(gSecret);
        initGrilleIncomplete(nbTrous, gSecret, gHumain);
        //saisirGrilleIncomplete(nbTrous, gOrdi);
        gOrdi = lireGrilleDeSudoku("grille1.txt");
        initPossibles(gOrdi, valPossibles, nbValPoss);
        return nbTrous;
    }  // fin initPartie

    //...........................................................
    // Tour du joueur humain
    //...........................................................

    /** pré-requis : gHumain est une grille de Sudoju incomplète pouvant se compléter en
     *               la  grille de Sudoku complète gSecret
     *
     *  résultat :   le nombre de points de pénalité pris par le joueur humain pendant le tour de jeu
     *
     *  action :     effectue un tour du joueur humain
     */
    public static int tourHumain(int [][] gSecret, int [][] gHumain) {
        int penalite=0;
        int valEntree;
        boolean sortir = true;
        int i;
        int j;

        do {

            System.out.println("saisissez les coordonnées i de la grille");
            i = saisirEntierMinMax(1, 9);
            i--;
            System.out.println("saisissez les coordonnées j de la grille");
            j = saisirEntierMinMax(1, 9);
            j--;

            if (gHumain[i][j] != 0)  {
                System.out.println("la case sélectionné n'est pas un trou");
            }

        } while (gHumain[i][j] != 0);

        do {
            System.out.println("entrez une valeur pour cette case (0 pour joker)");
            valEntree = saisirEntierMinMax(0, 9);

            if (valEntree != 0 && valEntree != gSecret[i][j]) {
                System.out.println("la valeur entrée n'est pas bonne " + gSecret[i][j]);
                penalite++;
            }

            System.out.println(valEntree != 0 && valEntree != gSecret[i][j]);

        } while (valEntree != 0 && valEntree != gSecret[i][j]);

        if(gHumain[i][j] == 0){ // l'utilisateur n'a pas rempli le trou, on considère qu'il prend un joker
            penalite++; // ajouter une pénalité
            gHumain[i][j] = gSecret[i][j];
        } else if (gHumain[i][j] == gSecret[i][j]) {
            System.out.println("la valeur est correcte !");
        }

        return penalite;
    }  // fin  tourHumain

    //.........................................................................

    // Tour de l'ordinateur
    //.........................................................................

    /** pré-requis : gOrdi et nbValPoss sont des matrices 9x9
     *  résultat :   le premier trou (i,j) de gOrdi (c'est-à-dire tel que gOrdi[i][j]==0)
     *               évident (c'est-à-dire tel que nbValPoss[i][j]==1) dans l'ordre des lignes,
     *                s'il y en a, sinon le premier trou de gOrdi dans l'ordre des lignes
     *
     */
    public static int[] chercheTrou(int[][] gOrdi,int [][]nbValPoss) {
        int[] T = new int[2];
        for (int i = 0; i < gOrdi.length; i++) {
            for (int j = 0; j < gOrdi[0].length; j++) {
                if (gOrdi[i][j] == 0 && nbValPoss[i][j] == 1) { // si il y a un trou (obligatoirement le premier donc)
                    T[0] = i;
                    T[1] = j;
                    return T; // on retourne les coordonnées du trou, ce qui arrête la fonction
                }
            }
        }
        return T; // return qui en théorie ne devrait pas être utilisé
    }  // fin chercheTrou

    //...........................return T;..............................................

    /** pré-requis : gOrdi est une grille de Sudoju incomplète,
     *               valPossibles est une matrice 9x9 de tableaux de 10 booléens
     *               et nbValPoss est une matrice 9x9 d'entiers
     *  action :     effectue un tour de l'ordinateur
     */

    public static int tourOrdinateur(int[][] gOrdi, boolean[][][] valPossibles, int[][] nbValPoss){
        int penalite=0;
        int[] coords = chercheTrou(gOrdi, nbValPoss);  // cherche les trous
        initPossibles(gOrdi, valPossibles, nbValPoss);  //initialise les valeurs ainsi que les valeurs qui sont possibles
        if(nbValPoss[coords[0]][coords[1]] == 1){  //Si il n'y a qu'une valeur possible
            return uneValeur(valPossibles[coords[0]][coords[1]]);     //return cette valeur qui est dans valPossibles //pour la prochaine fois erreur booléen != int
        }
        else {
            return penalite;               //sinon l'ordinateur passe son tour et ne complette pas le trou, il obtient un pénalité.

        }

    }  // fin tourOrdinateur

    //.........................................................................

    // Partie
    //.........................................................................



    /** pré-requis : aucun
     *  action :     effectue une partie de Sudoku entre le joueur humain et l'ordinateur
     *  résultat :   0 s'il y a match nul, 1 si c'est le joueur humain qui gagne et 2 sinon
     */
    public static int partie(){
        int[][] gSecret = new int[9][9];
        int[][] gHumain = new int[9][9];
        int[][] gOrdi = new int[9][9];
        boolean[][][] valPossibles = new boolean[9][9][10];
        int[][] nbValPoss = new int[9][9];

        int nbTrous = initPartie(gSecret, gHumain, gOrdi, valPossibles, nbValPoss);

        boolean partieFinie = false;
        int penHumain = 0;
        int penOrdi = 0;

        while (partieFinie == false) {
            afficheGrille(3, gHumain);
            penHumain = penHumain + tourHumain(gSecret, gHumain);
            System.out.println("tourHumain joué");

            afficheGrille(3, gOrdi);
            penOrdi = penOrdi + tourOrdinateur(gOrdi, valPossibles, nbValPoss);
            System.out.println("tourOrdi joué");


            partieFinie = true;
            System.out.println("check");

            for (int i = 0; i < gHumain.length; i++) {
                for (int j = 0; j < gHumain[i].length; j++) {
                    if (gHumain[i][j] == 0) {
                        partieFinie = false;
                    }
                }
            }
        }

        if (penHumain == penOrdi) {
            return 0;
        } else if (penOrdi > penHumain) {
            return 1;
        }

        return 2;

    }  // fin partie


    //.........................................................................


    /** pré-requis : aucun
     *  action :     effectue une partie de Sudoku entre le joueur humain et l'ordinateur
     *               et affiche qui a gagné
     */
    public static void main(String[] args){
        /**
        int[][]grille;
        Ut.afficher("Saisir un nombre de trous dans votre grille de sudoku.");
        int nbTrous=saisirEntierMinMax(0,81);
        saisirGrilleIncomplete(nbTrous,grille);
        initGrilleComplete(null);
        */

        System.out.println(partie());


        //suppValPoss(lamatrice, 4, 5, valPoss, nbvallposs);


        //________________________________________

    }  // fin main

} // fin SudokuBase
