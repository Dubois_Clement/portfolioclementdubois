// oh la pourquoi ça marche pas là 2222222

/**
 * La classe Carte représente une carte possèdant une figure répétée un certain nombre de fois avec une texture et une couleur.
 * On a besoin de connaître :
 * - La figure représentée,
 * - Le nombre de fois où la figure est représentée,
 * - La couleur de la figure,
 * - La texture de la figure.
 */
public class Carte  {
    private Figure figure;      //ajout
    public Couleur couleur;     //ajout
    public Texture texture;     //ajout
    public Texture nTexture;    //ajout
    public int nbFigure;    //ajout
    /**
     * Pre-requis : nbFigures > 0
     * Action : Construit une carte contenant nbFigures "figures" qui possèdent une "texture" et une "couleur"
     * Exemple : new Carte(Couleur.ROUGE, 2, Figure.OVALE, Texture.PLEIN) représente une carte contenant 2 figures ovales rouge et pleines
     */

    public Carte(Couleur couleur, int nbFigures, Figure figure, Texture texture) {
        this.couleur=couleur;
        this.nbFigure=nbFigures;
        this.figure=figure;
        this.texture=texture; //ajout
    }

    /**
     * Résultat : Le nombre de fois où la figure est représentée sur la carte.
     */

    public int getNbFigures() {
        return this.nbFigure;
        //throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");
    }

    /**
     * Résultat : La figure représentée sur la carte.
     */

    public Figure getFigure() {
        return this.figure;
        //throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");
    }

    /**
     * Résultat : La couleur représentée sur les figures de la carte.
     */

    public Couleur getCouleur() {
        return this.couleur;
        //throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");
    }

    /**
     * Résultat : La texture représentée sur les figures de la carte.
     */

    public Texture getTexture() {
        return this.texture;
        //throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");
    }

    /**
     * Action : compare les attributs de "this" et de "carte"
     * afin de déterminer si this est plus petit, égal ou plus grand que "carte"
     *
     * L'odre d'importance des attrbiuts est celui donné dans le constructeur (du plus prioritaire au moins prioritaire) :
     * Couleur, nombre de figures, figure, texture.
     * Pour comparer les couleurs, les figures et les textures, on utilisera leur position (ordinal) dans
     * leurs énumérations respectives.
     * Ainsi, pour toute paire {c1,c2} de Carte, c1 est inférieure à c2 si et seulement si
     * la valeur de c1 est inférieure à celle de c2 pour la caractéristique ayant la plus grande priorité
     * parmi celles pour lesquelles c1 et c2 ont des valeurs différentes.
     *
     *
     * Résultat :
     *  0 si "this" est égal à "carte"
     *  Un nombre négatif si "this" est inférieur à "carte"
     *  Un nombre strictement positif si "this "est supérieur à "carte"
     */

    public int compareTo(Carte carte) {
        Couleur couleurCarte = carte.getCouleur();
            if(this.couleur.ordinal() == couleurCarte.ordinal()){   //si les couleurs sont égales
                int nbFigureCarte = carte.getNbFigures();           //va chercher le nombre de figure de la carte 
                if(this.nbFigure == nbFigureCarte){                 //si les nombres de figure sont suppérieurs
                    Figure figureCarte = carte.getFigure();
                    if(this.figure.ordinal() == figureCarte.ordinal()){ //si les figures sont égale
                        Texture textureCarte = carte.getTexture();
                        if(this.texture.ordinal() == textureCarte.ordinal()){// si les textures sont égaux
                            return 0;
                        }
                        else if(this.texture.ordinal() < textureCarte.ordinal()){       //si le nombre de texture est supérieur à la carte this
                            return -1;
                        }
                        else{
                            return 1;
                        }
                    }
                    else if(this.figure.ordinal() < figureCarte.ordinal()){       //si le nombre de figure est supérieur à la carte this
                        return -1;
                    }
                    else{
                        return 1;
                    }
                }
                else if(this.nbFigure<nbFigureCarte){       //si le nombre de figure est supérieur à la carte this
                    return -1;
                }
                else{
                    return 1;
                }
            }
            else if(this.couleur.ordinal()<couleurCarte.ordinal()){ //si la couleur est supérieur à la carte this
                return -1;
            }
            else{
                return 1;
            }
            
        
        //throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");
    }

    /**
     * Résultat :
     * Une chaîne de caractères représentant la carte de la manière suivante :
     *  - Le texte est coloré selon la couleur de la carte
     *  - La chaîne de caractères retournée doit faire apparaitre toutes les caractériqtiques d'une carte sauf la couleur puisque le texte est affiché en couleur
     *  (Vous devez choisir une représentation agréable pour l'utilisateur)
     */

    @Override
    public String toString() {
        return this.nbFigure + " " + this.figure + " " + this.texture;
        //throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");
    }

}
