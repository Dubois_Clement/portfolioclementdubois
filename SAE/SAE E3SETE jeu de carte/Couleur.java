public enum Couleur {
    ROUGE("\u001B[31m"),
    VERT("\u001B[32m"),
    BLEU("\u001B[34m");

    private String couleur;

    Couleur(String c) {
        this.couleur = c;
    }

    public String getCouleur() {
        return this.couleur;
    }

    /**
     * Représente la couleur d'une Carte : jaune, rouge ...
     * En plus de donner une liste énumérative des couleurs possibles,
     * cette enumération doit permettre à la méthode toString d'une Carte de réaliser un affichage en couleur.
     */

}

