public enum Figure {

    OVALE("ovale"),
    TRIANGLE("triangle"),
    LOSANGE("losange");

    private String figure;

    Figure(String c) {
        this.figure = c;
    }

    public String getFigure() {
        return this.figure;
    }

    /**
     * Représente la figure (forme) d'une Carte : ovale , triangle ...
     */
}
